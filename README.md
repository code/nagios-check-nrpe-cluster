check\_nrpe\_cluster
====================

Run two or more NRPE checks and return a status based on their aggregated
results.

Thanks
------

This was written on company time with my employer [Inspire Net][1], who has
generously allowed me to open source it.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [MIT License][3].

[1]: https://www.inspire.net.nz/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/MIT
